# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.0] - 11/02/2021

- Update module to be D9-ready (passes d9-readiness scan)
- Add composer.json file
- Add changelog


## [2.0.0] - 07/11/2018

- Initialize stable version
